# eselect-clang

A slightly modified version of one of the usual modules to manage the
`clang` and `clang++` symlinks in Gentoo's `eselect`-system.

## Installation
Copy the file to `/usr/share/eselect/modules`.
